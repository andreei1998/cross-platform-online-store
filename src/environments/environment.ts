// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


const url1 ="https://foxshopz.xyz/wp-json/wc/v3"
const authUrl1 = "https://foxshopz.xyz/wp-json/jwt-auth/v1/token"
const tokenVerifyUrl1 ="https://foxshopz.xyz/wp-json/jwt-auth/v1/token/validate"



// noinspection DuplicatedCode
export const environment = {
  production: false,
  backend_api_url: url1,
  auth_url: authUrl1,
  token_verify_url: tokenVerifyUrl1,
  readOnlyKeys: {
    consumer_key: 'ck_4a7f21aab3006d68b63fc877d25707657f3834c4',
    consumer_secret: 'cs_1a40ead08fccce01c9b71cec437ff7a8e731e079'
  },

  writableKeys: {
    consumer_key: 'ck_c6daf734a987c2800bcece39436cf2e8335fa45e',
    consumer_secret: 'cs_4b0e4cebc6dbe796cd87988d3d2ad74c9465e962'
  },
    states: [
        {value: 'AN', name: 'Andaman and Nicobar Islands'},
        {value: 'AP', name: 'Andhra Pradesh'},
        {value: 'AR', name: 'Arunachal Pradesh'},
        {value: 'AS', name: 'Assam'},
        {value: 'BR', name: 'Bihar'},
        {value: 'CG', name: 'Chandigarh'},
        {value: 'CH', name: 'Chhattisgarh'},
        {value: 'DH', name: 'Dadra and Nagar Haveli'},
        {value: 'DD', name: 'Daman and Diu'},
        {value: 'DL', name: 'Delhi'},
        {value: 'GA', name: 'Goa'},
        {value: 'GJ', name: 'Gujarat'},
        {value: 'HR', name: 'Haryana'},
        {value: 'HP', name: 'Himachal Pradesh'},
        {value: 'JK', name: 'Jammu and Kashmir'},
        {value: 'JH', name: 'Jharkhand'},
        {value: 'KA', name: 'Karnataka'},
        {value: 'KL', name: 'Kerala'},
        {value: 'LD', name: 'Lakshadweep'},
        {value: 'MP', name: 'Madhya Pradesh'},
        {value: 'MH', name: 'Maharashtra'},
        {value: 'MN', name: 'Manipur'},
        {value: 'ML', name: 'Meghalaya'},
        {value: 'MZ', name: 'Mizoram'},
        {value: 'NL', name: 'Nagaland'},
        {value: 'OR', name: 'Odisha'},
        {value: 'PY', name: 'Puducherry'},
        {value: 'PB', name: 'Punjab'},
        {value: 'RJ', name: 'Rajasthan'},
        {value: 'SK', name: 'Sikkim'},
        {value: 'TN', name: 'Tamil Nadu'},
        {value: 'TS', name: 'Telangana'},
        {value: 'TR', name: 'Tripura'},
        {value: 'UK', name: 'Uttarakhand'},
        {value: 'UP', name: 'Uttar Pradesh'},
        {value: 'WB', name: 'West Bengal'}]
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.