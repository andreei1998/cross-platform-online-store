import { Component } from '@angular/core';
import { ToastController } from '@ionic/angular';
import {AuthService} from "../services/auth.service";

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {


  constructor(private authService: AuthService,
    private toastController: ToastController) {}

    async presentToast() {
      const toast = await this.toastController.create({
        message: 'Logged Out.',
        duration: 2000
      });
      toast.present();
    }
    
    logout() {
       this.authService.logout();
       this.presentToast();
    }
}
