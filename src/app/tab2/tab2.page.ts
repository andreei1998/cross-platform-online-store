import { Component, ElementRef, ViewChild } from '@angular/core';
import {NavController, Platform} from '@ionic/angular';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import "@alan-ai/alan-button";


@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  @ViewChild('alanBtnEl', {static:false}) alanBtnComponent: ElementRef<HTMLAlanButtonElement>;

  constructor(
    private platform: Platform,
    private navCtrl: NavController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {

  
    });
  }

  navigate(){
    this.navCtrl.navigateForward([`/search`]);
  }

    ngAfterViewInit() {}
      
}
