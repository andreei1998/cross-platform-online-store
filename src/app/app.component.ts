import {Component, ElementRef, ViewChild} from '@angular/core';

import {NavController, Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {Capacitor, Plugins} from "@capacitor/core";

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent {
    @ViewChild('alanBtnEl') alanBtnComponent: ElementRef<HTMLAlanButtonElement>;
    private greetingWasSaid: boolean = false;
    private visualState: any = {};
    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        public navCtrl: NavController,
    ) {
        this.initializeApp();
    }

    initializeApp() {
        /*this.platform.ready().then(() => {
             this.statusBar.styleDefault();
             this.splashScreen.hide();
           });*/

        this.platform.ready().then(() => {
            if (Capacitor.isPluginAvailable('SplashScreen')) {
                Plugins.SplashScreen.hide().then();
            }
        });
    }

    ngAfterViewInit() {
        // add event listener for connectionStatus,
        // when connection established we greet a user in the app
        this.alanBtnComponent.nativeElement.addEventListener('connectionStatus', (data) => {
            const connectionStatus = (<CustomEvent>data).detail;
            if (connectionStatus === 'connected') {
                if (!this.greetingWasSaid) {
                    // this.greetUserForFirstTime();
                    this.greetingWasSaid = true
                }
            }
        });
        this.alanBtnComponent.nativeElement.addEventListener('command', (data) => {
            const commandData = (<CustomEvent>data).detail;

    

        if (commandData.command === 'search') {
            console.log(commandData);
            this.navCtrl.navigateForward([`/${commandData.route}`]);
        }

        if (commandData.command === 'voice') {
            console.log(commandData);
            this.navCtrl.navigateForward([`/${commandData.route}`]);
        }

        if (commandData.command === 'cart') {
            console.log(commandData);
            this.navCtrl.navigateForward([`/${commandData.route}`]);
        }

        if (commandData.command === 'back') {
            console.log(commandData);
            this.navCtrl.navigateForward([`/${commandData.route}`]);
        }

        if (commandData.command === 'buy') {
            console.log(commandData);
            this.navCtrl.navigateForward([`/${commandData.route}`]);
        }
    });

    
    }


    async greetUserForFirstTime() {
        this.alanBtnComponent.nativeElement.componentOnReady().then(async () => {
            try {
                await this.alanBtnComponent.nativeElement.activate();
                this.alanBtnComponent.nativeElement.callProjectApi("greet", {}, () => { });
            } catch(e) {
                console.info('DEBUG', e);
            }
        });
    }
}
